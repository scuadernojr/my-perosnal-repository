#include <iostream>
using namespace std; 
class Deep{
private:
    int *data; 
public:
    void set_data_value (int d) {*data = d;}
    int get_data_value () {return *data; }
    
    //constructor 
    Deep (int d); 
    //Copy constructor
    Deep (const Deep &source);
    //Deconstructor
    ~Deep();
};
Deep::Deep(int d){
    data = new int; 
    *data = d; 
}
// Deep::Deep(const Deep &source){
//     data = new int; // allocated storage in the Heap
//     *data = *source.data; 
//     cout << "Copy constructor - DEEP " << endl; 
// }

//Delegate Copy constructor
Deep::Deep(const Deep &source)
    :Deep {*source.data}{
        cout << "Copy constructor -- deep" << endl; 
    }

Deep::~Deep(){
    delete data; 
    cout << "Deconstructor freeing Heap" << endl; 
}
void display_deep (Deep s){
    cout << s.get_data_value() << endl; 
}

int main(){
    Deep obj1 {100}; 
    display_deep (obj1);
    
    obj1.set_data_value(10000);
    Deep obj2 {obj1}; 


    return 0;
}
#include <iostream>
#include <string>
using namespace std;  
class Player{
public:
    string name; 
    int health; 
    int xp;  

    void talk(string text_to_say) {cout << name << " says " << text_to_say << endl;};
    bool is_dead(); 


}; 
class Account{
public: 
    string name; 
    double balance;

    bool deposit( double bal) {balance += bal; cout << " In deposit ";}
    bool withdraw (double bal) {balance -= bal; cout << " Withdrawing " << endl;}
};
int main(){
    Account sammys_account; // account OBJECT named Sammys_account 
    sammys_account.name = "Sammys Account ";
    sammys_account.balance = 2000.00;
    sammys_account.deposit (1000);  
    sammys_account.withdraw (500); 

    cout << "The name of the accoutn your are accessing is " <<  sammys_account.name << endl; // How to show the value 

    Player sammy; 
    sammy.name = "Sammy"; 
    sammy.health = 28; 
    sammy.xp = 100; 
    sammy.talk ("Hi there"); 

    Player *enemy = new Player;
    enemy->name = "Enemy"; 
    enemy->health = 30; 
    enemy->xp = 100; 

    enemy-> talk("I will destroy you"); 
    delete enemy; // clear storage in the HEAP!  
    return 0; 
}
#include <iostream>
using namespace std; 

class Player{
private: 
    std::string name; 
    int health;
    int xp; 
public:
    std::string get_name(){return name;}
    int get_health() {return health;}
    int get_xp () {return xp; }
    Player(std::string name_val = "None", int health_val = 0, int xp_val = 0); 
    
    // Copy constuctor
    Player (const Player &source); 
    //Destructor
    ~Player () {cout << "Destructor is being called fore "+ name << endl;}
};
Player::Player (std::string name_val, int health_val, int xp_val)
    :name{name_val},health{health_val}, xp {xp_val}{ // inistalize the player right away before the body executes
        cout << "Three args constructor " + name << endl; 
    }
Player::Player (const Player &source)//COpy constructor
    :name(source.name), health(source.health), xp(source.xp){
        cout << "Copy constructor made a copy of " << source.name << endl; 
    }
void display_player(Player p){
    cout << "Name is " << p.get_name()<< endl;
    cout << "Health is " << p.get_health()<< endl;
    cout << "XP is " << p.get_xp()<< endl;

}
int main(){
    
    
    Player empty;
    display_player (empty); 
    Player Sammy {"Sammy"} ; 
    Player hero {"Hero", 100}; 
    Player villan{"Villan, 100, 20"}; 
    

    return 0;

}
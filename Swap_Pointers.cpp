#include <iostream>
using namespace std;
void swap(int *, int *);
int main(){


    int i {5};
    while (i > 0){
        cout << i << endl;
        i--;
    }
    
    int x {100}, y {200}; 
    cout << "\nx: " << x << " and Y: " << y << endl; 
    
    swap(&x,&y); 
    cout << x << " " <<  y << endl;  
    return 0;
}
void swap(int *a, int *b){
    int temp = *a; 
    *a = *b; 
    *b = temp; 
}
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> test_scores (10,5);

    for(int i = 0; i <10; i++)
        cout << "\nTest score " << i << "   " << test_scores[i];

    vector<int> vector1 = {1, 2, 3, 4, 5};
    cout << endl; 
    cout << "Index 0 has a value of " << vector1.at (0) << endl;
    cout << "Index 4 has a value of " <<vector1.at (4) << endl;

    cout << "=========================================================" << endl;

    cout << "Vector 1 has elements " << endl; 
    for (auto i : vector1)
     cout << i;   

    for (int k = 0; k < 5; k++)
    cout << "\nvector " << k << "  " << vector1.at(k); 

cout << "\n============================================================" << endl; 
  
    vector<int> my_vector {10,20,30,40,50}; 
    
    cout << my_vector.at(0) << " using .AT command"  << endl; 
    cout << my_vector[0] << " using ARRAY Notation" << endl; 
    
    for (int k = 0; k < my_vector.size(); k++)
        cout << my_vector[k] << " ";
             
    my_vector.push_back(60); // adding 60 to end of vector 
    
    cout << my_vector[5] << endl; 
    
    for (int i = 0; i < my_vector.size(); i++)
        cout << my_vector[i] << " ";    
        

    return 0;
}

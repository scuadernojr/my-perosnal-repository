#include <iostream>
#include <vector>
#include <string>
using namespace std;
void display(vector<string> *v);
void display(int *array, int sentinel);  
int main(){

    vector<string> stooges {"Sammy", "Kaitlyn", "Sheilla"}; 
    display (&stooges); 

    cout << "\n==========================" << endl; 
    int array[] {1, 2, 9, 20, 21, -8 };
    display( array, -8 ); // DO not need & for arrays 
    
    return 0;
}
void display(vector<string> *v){
    for (auto val : *v)
        cout << val << " "; 
    cout << endl; 
}
void display(int *array, int sentinel){
    while (*array != sentinel)
        cout << *array++ << " "; 
    cout << endl; 
}
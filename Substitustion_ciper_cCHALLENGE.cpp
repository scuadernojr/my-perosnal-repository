#include <iostream>
#include <string> 
using namespace std;
int main(){
    string secret_message {}; 
    string alphabet {"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"};
    string key {"lkjhgfdsapoiuytrewqmnbvcx5LKJHGFDSAPOIUYTREWQMNBVCX5"}; 

    cout << "Enter a secrect message: "; 
    getline(cin, secret_message); 

    string encrypted_message {}; 

    for (char c : secret_message){
        size_t pos = alphabet.find(c); 
        if (pos != string::npos){
            char new_char {key.at(pos)};
            encrypted_message += new_char; 
        }else {
            encrypted_message += c; 
        }
    }
    cout << "\n******Encrypted message*******" << endl;
    cout << encrypted_message << endl;  

    cout << "\n===============DECRYPTING MESSAGE===========================" << endl; 

    string decrypted_mesage;

    for (char c : encrypted_message ){
        size_t pos = key.find(c); 
        if (pos != string::npos){
            char new_char {alphabet.at(pos)};
            decrypted_mesage += new_char; 
        }else {
            decrypted_mesage += c; // Not going to substitue character 
        }
    } 
    cout << "\n******Decrypted message******" << endl;
    cout << decrypted_mesage << endl;  
     
    return 0;
}
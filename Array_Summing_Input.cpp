#include <iostream>
using namespace std; 
int main (){
    int my_array[5] {};
    int sum {0}; 
    int size_of_array = sizeof(my_array)/sizeof(my_array[0]); // size of the array using starting from index 0 
    

    cout << "Enter numbers for array: " << endl; 
    for (size_t i = 0; i < size_of_array; ++i){
        cin >> my_array [i]; 
        sum += my_array [i]; 
    } 
    int average = sum/ size_of_array; 

    cout << "Sum of the numbers in the array are " << sum << endl; 
    cout << "Average of the numbers is: " << average << endl; 

    cout << "\n====================ELEMENTS IN ARRAY===================" << endl; 
    for (auto val : my_array)
        cout << val << " "; 
    cout << endl;  
    return 0;
}
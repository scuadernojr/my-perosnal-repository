#include <iostream>
#include <vector>
#include <string> 
using namespace std; 
int main (){
    int score {100};
    int *score_ptr {&score}; 

    cout << *score_ptr << endl; // 100 dereferencing pointer  

    *score_ptr = 200; // asigning score_ptr to 200 
    cout << *score_ptr << endl; // 200 
    
    cout << "====================================================" << endl; 
    // int *ptr1 {nullptr}; 
    // double *p2 {nullptr}; 
    // unsigned long long *p3 {nullptr}; 
    // vector<string> *p4 {nullptr}; 
    // string *p5 {nullptr}; 

    // cout << "Value: " << ptr1 << endl; 
    // cout << "Size of: " << sizeof(ptr1) << " bytes" << endl; // will be 8 for all types ex. string, int, double 
    // cout << "Address: " << &ptr1 << endl; 
    // cout << "Pointing at: " << ptr1 << endl; 

//     cout << "\n====================================================================" << endl; 
//     int score {10}; 
//     double high_temp {100.72}; 

//     int *score_ptr {nullptr}; 
//     score_ptr = &score; 

//     cout << "Value of Score: " << score << endl; 
//    // cout << "SizeOf score: " << sizeof(score) << endl;
//     cout << "Address of score: " << &score << endl;  
   
//     cout << "Value of score_ptr: " << score_ptr << endl;
//     // cout << "SizeOf score_ptr: " << sizeof(score_ptr) << endl; 
//     // cout << "Address of score_ptr: " << &score_ptr << endl;  

    return 0; 
}
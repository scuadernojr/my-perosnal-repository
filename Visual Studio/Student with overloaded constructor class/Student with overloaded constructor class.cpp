// Student with overloaded constructor class.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
using namespace std; 

class Student {
private:
    string name; 
    int age = 0; 
public:
    Student (string a) :name(a){}
    Student(string a, int b) {
        name = a;
        age = b; 
    }
    void Display() {
        cout << "Name: " << name << endl;
        cout << "Age: " << age << endl; 
    }
    void setName(string a) { name = a; }

};
int main()
{
    Student stud1("Sammy" , 12 );
    stud1.Display();

    
    cout << "=================================================" << endl;

    Student stud2("Jimbo"); // using overloaded constructor with 1 Variable
    stud2.Display(); 

    cout << "=================================================" << endl;

    Student stud3("", 12); // using overloaded constructor with 1 Variable
    stud3.Display();

    cout << "=================================================" << endl; 
    Student *enemy = new Student("Enemy", 99); // Creating object pointer 
    enemy->Display();  // Dereferencing Pointer 
    delete enemy; 



}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

#include <iostream>
#include <cmath> 
using namespace std; 

int amount_of_calories_of_carbs (); 
int main () {
    
    int carbs{}, fats{}, proteins {}; 
    const int calories_for_carbs {4}; 
    const int calories_for_proteins {4};
    const int calories_for_fats {9};

    cout << "CARBS consumed? " ; 
    cin >> carbs;
    cout << "FATS consumed? ";
    cin >> fats; 
    cout << "PROTEINS consumed? "; 
    cin >> proteins;

    int calories_per_carbs {}; 
    int calories_per_fats {};
    int calories_per_proteins {};

    calories_per_carbs = (carbs * calories_for_carbs) ; 
    calories_per_fats = (fats * calories_for_fats) ; 
    calories_per_proteins = (proteins * calories_for_proteins); 

    int total_calories {}; 
    total_calories = calories_per_carbs + calories_per_fats + calories_per_proteins; 

    cout << "The TOTAL calories consumed should be " << total_calories << endl; 
    





    return 0;
}

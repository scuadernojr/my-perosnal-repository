#include <iostream> 

using namespace std; 

int main(){
    
    char letter_grade {}; 
    
    cout << "Enter letter grade ";
    cin >> letter_grade; 
    
    switch (letter_grade){
        case 'A' : 
        case 'a' : 
            cout << "You need a 90% or above " << endl ; 
            break;
        case 'B':
        case 'b':
            cout << "You need a 80 - 89" << endl;
            break; 
        case 'C': 
        case 'c': 
            cout << "You need a 70 - 80 " << endl; 
            break; 
        case 'f':
        case 'F':
        {
            char confirm {}; 
            cout << "Are you sure Y/N" <<endl; 
            cin >> confirm; 
            
            if (confirm == 'y'  || confirm == 'Y')
                cout << "You FAILED" << endl; 
            else if (confirm == 'n' || confirm == 'N')
                cout << "good go study" << endl; 
            else 
                cout << "Invalid choice" << endl; 
            break; 
        }
        default : 
            cout << "Not Valid grade" << endl; 
        
    }
    return 0;
}
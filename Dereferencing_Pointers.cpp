#include <iostream>
#include <string>
#include <vector> 
using namespace std; 
int main(){
vector<string> stooges {"Sammy", "Sheilla", "Kaitlyn"}; 
vector<string> *vector_ptr {nullptr};

vector_ptr = &stooges; 

cout << "Using VECTOR notation " << stooges.at(0) << endl;
cout << (*vector_ptr).at(0) << endl; // pointer has to be in ()

cout << "\n==================Pointer for loop====================" << endl; 
for( auto k : *vector_ptr)
    cout << k << " "; 


return 0;
}

#include <iostream>
using namespace std; 
void print (const int *const arr, size_t size);
int main(){
    const size_t array_1_size {5}; 
    const size_t array_2_size {3}; 

    int array1[] {1,2,3,4,5}; 
    int array2[] {10, 20, 30}; 

    cout << "Array 1: "; 
    print(array1,array_1_size); 

    cout << "\nArray 2: " ; 
    print(array2, array_2_size); 
    return 0; 
}
void print (const int *const arr, size_t size ){
    for (size_t i = 0; i < size; ++i)
        cout << arr[i] << " "; // can use *(arr +i)
}
#include <iostream>

using namespace std; 

int main(){
    
    int i {1};
    //while loop divisible by 2 
    while (i <=10){
        if (i % 2 == 0 )
            cout << i << endl;
            i++; 
    }
    
    // displaying scores with wile loop
    int scores [] {100, 90, 80};
    
    int j = 0; 
    while (j < 3){
        cout << scores [j] << " "; 
        ++j;
    }
     cout << endl; 

    cout << "\nplease enter a number "; 
    int number{}; 
    
    
    cin >> number; 
    
    while (number >= 100){ 
        cout << "Enter number LESS than 100: "; 
        cin >> number; 
    }
    
    cout << "Number you chose is " << number; 
    
    cout << "\n===================================================================" << endl; 
    
    bool done {false};
    int num1 {0};
    
    while (!done){
        cout << "Enter number between 1 and 5 "; 
        cin >> num1; 
        
        if (num1 <= 1 || num1 >= 5)
            cout << "out of range, try again "; 
        else {
            cout << "thanks" << endl;
            done = true; 
        }
    }
    
    return 0;
    
}
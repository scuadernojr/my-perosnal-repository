#include <iostream>
#include <vector> 
using namespace std; 

int main(){
    
    const int max {10};
    const int min {1}; 
    
    
    for (int num1 {min}; num1 <= max; num1++){
        for (int num2 {min}; num2 <= max; num2++){
            
            //int result = 0;
            int result = num1 * num2; 
            
            cout << num1 << " * " << num2 << " = " << result << endl;      
        }
        cout << "==============================" << endl ; 
    }  
    

    cout << "======================================================================================="<< endl; 
    int num {};
    
    cout << "Enter a number ";  
    cin >> num; 
    
    vector<int> data {}; 
    
    for (int i = 1; i <= num; i++){
        int data_item {}; 
        cout << "Enter data item " << i << ": "; 
        cin >> data_item; 
        data.push_back(data_item); 
    }
    
    //checking elements in the DATA vector 
    // for (auto val : data)
    //     cout << val << " " ; 
        
    cout << "\nDisplaying * =============================================" << endl; 
    
    for (auto val  : data){
        for (int i = 1; i <= val; i++){
            cout << "*"; 
        }
        cout << endl; 
    }
    return 0; 
}
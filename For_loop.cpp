#include <iostream>

using namespace std;

int
main ()
{

    int my_array [5] {10,20,30}; 
    
    for (int i =0; i <= 4; i++)
        cout << "Index "<< i << " contains: " << my_array [i] << endl;
    
    cout << "\n==================================================" << endl;    
    
    for (int k =0; k < 5; k++) 
        cout << "Index "<< k << " contains: " << my_array [k] << endl;
        
    cout << "\n====================================================\n" ; 
    
    for (int j = 1 , l = 5; j <= 5; j++, l++ )
        cout << j << " + " << l << " = " << j + l << endl; 
    
    for (int m = 1; m <= 100; m++){
        cout << m; 
        
        if (m % 10 == 0)
            cout << endl;
        else 
            cout << " "; 
    }
        
  return 0;
}

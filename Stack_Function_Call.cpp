#include <iostream>
using namespace std; 

int function(int, int); 
void function2( int &, int, int); // 

int main(){
    int z {}, x {10}, y {20};

    z = function(x,y); // calls function with parameters x = 20 and y = 10 
    cout << "Z is " << z << endl;  // Result with REF is 60 without is 30 
    return 0; 
}
int function(int a, int b){
    int result {}; 
    result = a + b; // a = 10 : b = 20 : RESULT = 30  
    function2(result, a, b); // call function 2 
    return result;
}
void function2( int &x, int y, int z) {
    x += y + z; // x is a reference so will UPDATE RESULT 
    
}
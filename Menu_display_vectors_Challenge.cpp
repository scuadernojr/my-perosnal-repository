#include <iostream>
#include <vector> 
#include <algorithm>

using namespace std;

int main (){
    
    char selection {}; 
    vector<int> numbers {}; 
    
    do {
        cout << "\n=============================" << endl; 
        cout << "P- Print nubers" << endl; 
        cout << "A- Add number" << endl; 
        cout << "M- Display mean of number" << endl; 
        cout << "S- Display smallest number" << endl; 
        cout << "L- Display largest number" << endl; 
        cout << "Q- QUIT" << endl;
        cout << "\n=============================" << endl; 
        cout << "\nENter your choice: " ; 
        cin >> selection; 
        
        if (selection == 'p' || selection == 'P'){
            if(numbers.size() == 0)
                cout << "List is empty" << endl;
            else {
                cout << "[ ";
                for (auto val : numbers)
                    cout << val << " "; 
                cout << " ]" << endl; 
            }
        }else if (selection == 'A' || selection == 'a'){
            int num_add {};
            cout << "Enter a number to add "; 
            cin >> num_add; 
            numbers.push_back(num_add);
            cout << num_add << " was added to the array" << endl; 
            
        }else if (selection == 'M' || selection == 'm'){
            int sum{0};
            int mean{};
            for(auto val : numbers)
                sum += val;
                //static cast used to convert int to double

            //mean = static_cast<double> (sum) / numbers.size(); 

            cout << "The mean is " << static_cast<double> (sum) / numbers.size() << endl; 
        }else if (selection == 'S' || selection == 's'){
            int min {}; 
            //*min_element and *max element need #include <algorithm>
            min = *min_element(numbers.begin(), numbers.end());
            cout << "The minimum value is " << min << endl; 
        }else if (selection == 'L' || selection == 'l'){
            
            // ANother way to do largest 
            int largest = numbers.at(0);
            for (auto num : numbers)
                if(num > largest)
                    largest = num; 
                cout << "Largest number is " << largest << endl; 

            // int max {};
            // max = *max_element(numbers.begin(), numbers.end());
            // cout << "The largest number is " << max << endl;

        }
        
    }while (selection != 'q' && selection != 'Q');{
        cout << "Good Bye!" << endl; 
    }
    
    return 0;
}
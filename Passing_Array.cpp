#include <iostream>
using namespace std; 
void print_array(const int arr[], size_t size); // print array of int and size of the array
void set_array(int arr[], size_t size, int value); // set each value to the value 

void print_array(const int arr[], size_t size){// set array as const 
    for (size_t i {0}; i < size; ++i)
        cout << arr[i] << " "; 
    cout << endl; 
}
void set_array(int arr[], size_t size, int value){
    for (size_t i = 0; i < size; ++i)
        arr[i] = value; 
}
int main (){
    int my_scores[] {100,90,80,70,60}; 
    print_array (my_scores, 5); 
    set_array (my_scores, 5 , 100); 
    print_array (my_scores, 5); 
    return 0; 
}
#include <iostream>
using namespace std;

void can_you_drive(int age, bool has_car); 
int sum (int num1, int num2); 
void say_world();
void say_hello(); 
int main (){
    
    int a {};
    bool h {false}; 
    
    cout << "Enter your age " << endl;
    cin >> a; 
    
    cout << "Do you have a car? "; 
    cin >> h;
    
    can_you_drive(a, h);

    cout << "\n====================Other VOID Examples=========================" << endl; 
    cout << sum(2,4) << endl;
    cout << sum (2,1) << endl;
    cout << sum(-1,4) << endl; 
    say_hello(); 
    return 0;
}

void can_you_drive(int age, bool has_car) {
    
    const int driver_age {16};
    
    if (age >= driver_age)
        if (has_car)
            cout << "Yes - you can drive!";
        else 
            cout << "Sorry, you need to buy a car before you can drive!"; 
    else 
        cout << "Sorry, come back in " <<  driver_age - age << " years and be sure you own a car when you come back."; 
    
    

}
int sum(int a, int b){
    cout << boolalpha;  
    if (a > -1 && b > -1)
        return a + b;
    else 
        return false; 
}
void say_world (){
    cout << " world";
}
void say_hello (){
    cout << "Hello";
    say_world(); 
     
}

#include <iostream>
#include <cmath>
#include <vector>
#include <numeric> // used for accumulate for vectors 
#include <iomanip> // used for set precision 

using namespace std;


template<typename T>

double get_average(std::vector<T> const& v) {
    if (v.empty()) {
        return 0;
    }
    return std::accumulate(v.begin(), v.end(), 0.0) / v.size();
}
int main (){

    vector<double> num_of_fats {}; 
    double food_items;
    
    cout << "How many food items conatin fats? "; 
    cin >> food_items; 
    
    cout << "Enter the amount of fats:" << endl; 
    for (size_t i = 0; i < food_items ; ++i){
        double input; 
        cin >> input; 
        num_of_fats.push_back(input); 
    }
    
    cout << setprecision(2) << endl; 

    double avg = get_average(num_of_fats); 
    cout << "Average is: " << avg << endl; // using template 
    cout << "Sum is: " << accumulate(num_of_fats.begin(), num_of_fats.end(), 0.0); 

    
    // double avg1 = get_sum(num_of_fats); 
    // cout << "Sum of fats is " << avg1 << endl; 
   
   
   
    // for (auto fats : num_of_fats){
    //     cout << fats << " ";
    //     double sum += fats; 
    // }
    //cout << "Fats added together for " << food_items << " food items is " << sum << endl; 
    return 0;
}
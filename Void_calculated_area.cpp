#include <iostream>
#include <cmath> 
#include <iomanip> 
using namespace std;
const double pi = 3.14159; 
void area_of_circle(); 
double calculated_area(double x);

int main(){
    area_of_circle(); 
    return 0; 
}
void area_of_circle(){
    double radius {}; 
    
    cout << "Please enter a radius: "; 
    cin >> radius; 

    cout << fixed << setprecision(2); // set decimal to 2 places 
    cout << "A circle with a radius of " << radius << " has an area of " << calculated_area(radius)<<  endl; 
    
}
double calculated_area(double x){
    double area {};
    area = pow(x,2) * pi; 
    return area; 
}


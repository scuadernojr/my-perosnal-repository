#include <iostream>

using namespace std;

int main() {
    int generated_test_scores [5] {100, 200, 300, 400};

    cout << "\n\nGenerated test score index 0:\n" << generated_test_scores [0];
    cout << "\nGenerated test score at index 1:\n" << generated_test_scores [1];
    cout << "\nGenerated test score 3:\n" << generated_test_scores [3];
    cout << "\nGenerated test score 7: DOES NOT EXSIST\n" << generated_test_scores [7];
    
    cout << "\n==========================USER INPUT================================" << endl; 
    
    int user_test_scores [5];

    cout << "\nPlease enter 5 test scores\n"; 
    for (int k =0; k < sizeof(user_test_scores)/sizeof(user_test_scores[0]); k++)
        cin >> user_test_scores [k]; // for loop USER INPUT 
    
    for (int i = 0; i < 5; i++)
        cout << "\nTest score at index " << i << " is " << user_test_scores [i];
    
    cout << "\n========================LENGTH/SIZE OF ARRAY========================"<< endl; 
    int length = sizeof(user_test_scores)/sizeof(user_test_scores[0]); 
    cout << "SIZE or LENGTH of Array is: " << length << endl; 
    return 0;
}

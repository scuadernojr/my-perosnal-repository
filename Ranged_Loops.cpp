#include <iostream>
#include <vector> 

using namespace std; 

int main(){
    
    int my_array[] {100, 90, 80}; 
    
    for (int a : my_array) 
        cout << a << endl; //Prints out element
     
    for (auto &a : my_array)
        cout << my_array << endl; // Pritns out address  
        
    
    vector<int> my_vector {20, 30, 40};
    
    for (auto a : my_vector)
        cout << a << " "; 
      
    // Printing string characters
    string str = "Geeks";
    for (char c : str) 
       cout << c << ' ';
          
    cout << '\n';          
    
    return 0;
    
}
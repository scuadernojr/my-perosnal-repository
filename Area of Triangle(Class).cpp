#include <iostream>
#include <cmath> 
#include <iomanip> 
using namespace std; 

class Triangle{
    public: 
    void area(int x, int y, int z);
    void perimeter(int x, int y, int z); 

}; 
void Triangle::area(int x, int y, int z)
{
    	float s=((x+y+z)/2.0), Area=sqrt(s*(s-x)*(s-y)*(s-z));
	    cout<< "The area of a triangle is " << Area <<" squared\n";
}

void Triangle::perimeter(int x, int y, int z)
{
    int perimeter = x+y+z; 
    cout << "The perimeter of the triangle is " << perimeter << endl; 
}

int main(){
  
   Triangle triangle1; 
   int s1= 3;
   int s2 = 4; 
   int s3 = 5; 
   triangle1.area (s1,s2,s3); 
   triangle1.perimeter(s1,s2,s3);
   
}

#include <iostream>
#include <string>
using namespace std; 
int main(){

    string s2 {}; 
    getline(cin,s2); // user input with white spaces 
    cout << "User input was: " << s2 << endl; 

    cout << "Size: " << s2.size() << endl;// find the size of string
    cout << "Substring: " << s2.substr(0,4) << endl; //display from from index 0 to 4 
    cout << "\n===========INSERT()===============" << endl;

    s2.insert(7, "xx");// Insert index 6 with xx  
    cout << s2 << endl; 

    cout << "\n=========FOR LOOP============\n\t"; 
    for (size_t i = 0; i < s2.length(); ++i)
        cout << s2.at(i); 
    cout << "\n===========RANGED LOOP=========\n"; 
	for (auto i: s2)
        cout << i ;
    cout << "\n===========.find()=========\n";
    string s1 = {"The secrect word is boo"}; 
    string word = {};

    cout << "Enter a  word or phrase "; 
    getline(cin, word); 
    
    size_t pos = s1.find(word); // find te word in OBJECT s1. SIze_t for zero or greater 
    if (pos != string::npos)
        cout << "Found " << word << " at position: " << pos << endl;
    else 
        cout << "Sorry, didnt find the word" << endl; 


    
    
    return 0;
}
void cpp_strings() {
    
    string journal_entry_1 {"Isaac Newton"};
    string journal_entry_2 {"Leibniz"};
    
    //----DO NOT MODIFY THE CODE ABOVE THIS LINE----
    //----WRITE YOUR CODE BELOW THIS LINE----
    
    journal_entry_1.erase(0,6);
    
    if (journal_entry_1 > journal_entry_2)
        journal_entry_1.swap(journal_entry_2);
        
    
    //----WRITE YOUR CODE ABOVE THIS LINE----
    //----DO NOT MODIFY THE CODE BELOW THIS LINE----
    
    cout << journal_entry_1 << "\n" << journal_entry_2;
}
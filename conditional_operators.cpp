#include <iostream>

using namespace std;

int
main ()
{

    int num {}; 
    
    cout << "Please enter a number " ; 
    cin >> num; 
    
    if (num %2 == 0)
        cout << "number is even"; 
    else 
        cout << "number is odd" << endl;
        
    cout << num << " is " << ((num %2 == 0 ? "even" : "odd")); 
 
    cout << "/n=======================================================" << endl;

    int num1 {}; 
    int num2 {};
    
    cout << "Enter two numbers "; 
    cin >> num1 >> num2;
    
    if (num1 != num2){
        cout << "Largest " << ((num1 > num2) ? num1 : num2) << endl;
        cout << "smallest " << ((num1 < num2) ? num1 : num2); 
    }
    else
        cout << "Numbers are the same" << endl; 
 
  return 0;
}

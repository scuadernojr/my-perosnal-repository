#include <iostream>
using namespace std; 
int main () {
    
    int *p1 {nullptr}; 
    p1 = new int; // allocated in the HEAP 
    cout << p1 << endl; 
    delete p1; 

    size_t size{0}; 
    double *temp {nullptr}; 

    cout << "How many temps do you want to store: "; 
    cin >> size; 

    temp = new double[size]; // name of pointer 

    cout << temp << endl; // area in the heap 

    delete [] temp; //name of pointer 
    return 0;
}
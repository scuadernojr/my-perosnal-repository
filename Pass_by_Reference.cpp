#include <iostream> 
using namespace std;

void swap(int &, int &);
void swap_without_ref (int a, int b);
int main (){
    int x = 20;
    int y = 30; 
    cout << "X is " << x << "\nY is " << y << endl; 
    swap(x,y); 
    cout << "=======================SWAP=======================" << endl; 
    cout << "X was 20 but is now " << x << "\nY was 30 but is now " << y << endl;
    swap(x,y); 
    cout << "=======================SWAP=======================" << endl;
    cout << "X: " << x << endl;
    cout << "Y: " << y << endl; 
    cout << "=======================SWAP_WITHOUT_REF=======================" << endl;
    swap_without_ref(x,y);
    cout << "X without REF is " << x << endl;
    cout << "Y without REF is " << y << endl;  
    return 0;
}

void swap (int &x, int &y){
    int temp = x; 
    x = y; 
    y = temp; 
}
void swap_without_ref (int a, int b){
    int temp = a; // x 20 is = temp 
    a = b; // y is 30 = x 
    b = temp; 
}
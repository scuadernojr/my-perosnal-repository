#include <iostream>
using namespace std; 
int main (){
    int scores [] {100, 200, 300}; 
    int *p1 {nullptr}; // can do int *p1 {scores}; 
    p1 = scores ; 
    
    cout << "\n==================ARRAY SUBSCRIPT NOTATION=======================" << endl; 
    cout << scores[0] << endl;// 100

    cout << "\n==================POINTER SUBSCRIPT NOTATION=======================" << endl;
    cout << p1[2] << endl; // 300

    cout << "\n==================POINTER OFFSET NOTATION=======================" << endl;
    cout << *p1 << endl; // 100 
    cout << *(p1 + 1) << endl; // 200 

    cout << "\n==================ARRAY OFFSET NOTATION=======================" << endl;
    cout << *scores << endl; // 100 
    cout << *(scores + 1) << endl; // 200 
    return 0;
}
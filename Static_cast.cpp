#include <iostream>

using namespace std;

int main() {

    int num1 {}, num2 {}, num3 {}; 
    const int x = {3}; 
    
    cout << "Enter three numbers "; 
    cin >> num1 >> num2 >> num3; 
    
    int sum {0}; 
    double average {0.0}; 
    
    sum = {num1 + num2 + num3}; 
    
    
    average = static_cast<double> (sum)/x; //static_cast to convert INT to DOUBLE 
    
    cout << "The sum of the three nunbers is " << sum << endl;
    cout << "The average WITH static_cast is " << average << endl; 
    
    average = sum/x; 
    
    cout << "The average WITHOUT static_cast is " << average << " which is WRONG" << endl; 
    
    
  return 0;
}
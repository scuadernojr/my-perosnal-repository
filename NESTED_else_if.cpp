#include <iostream>

using namespace std;

int
main ()
{

    int exam_score {}; 
    const int min {0};
    const int max {100};
    
    cout << "Exam score: " << endl;  
    cin >> exam_score; 
    
    char letter_grade {}; 
    
    if (exam_score >= min && exam_score <= max) {
        if (exam_score >= 90)
            letter_grade = 'A'; 
        else if (exam_score >= 80)
            letter_grade = 'B'; 
        else if (exam_score >= 70)
            letter_grade = 'C';
        else if (exam_score >= 60)
            letter_grade = 'D'; 
        else
            letter_grade = 'F'; 
                cout << "\nyour grade is " << letter_grade << endl;
        
        if (letter_grade == 'F')
            cout << "You need to retake course" << endl;
        else
            cout << "You passed " << endl; 
    
    }else {
        cout << "Sorry, number you entereed is not in range" << endl; 
    }
  return 0;
}

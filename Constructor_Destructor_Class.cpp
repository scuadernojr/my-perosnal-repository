#include <iostream>

using namespace std; 
class Base {
private: 
    int value; 
public:
    Base()
    : value {0} {
        cout << "Base no args Constructor being called" << endl; 
    }
    Base(int x) :value {x} {
        cout << "Overloaded constructor being called" << endl; 
    }
    ~Base () {
        cout << "Base Destructor being called" << endl; 
    }
};
class Derived : public Base{
    using Base::Base; // Inhertiting the Base (int x) Constructor 
private:
    int double_value;
public:
    Derived() :double_value {0}{
        cout << "Derived No args Constructor" << endl; 
    }
    Derived (int x) : double_value {x*2}{
        cout << "Overloaded Derived Constructor being called" << endl; 
    }
    ~Derived (){
        cout << "Derived DESTRUCTOR being called" << endl; 
    }

};

int main(){
    Base a; 
    Base b {100}; 
    Derived c; 
    return 0; 
}
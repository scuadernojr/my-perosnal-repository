#include <iostream>
#include <vector>
#include <string>
using namespace std;
int main (){

    vector<string> stooges {"Sammy", "Kaitlyn", "Sheilla"}; 

    for (auto &str : stooges){ // & will change the vector string to Hello 
        str = "HELLO"; 
        cout << str << " "; 
    }
    cout << endl; 
    for (auto const &str : stooges) // added const 
        cout << str << " "; 
    cout << endl; 
    return 0;
}
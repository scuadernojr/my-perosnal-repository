#include <iostream>
#include <string>
#include <vector> 
using namespace std; 

void print(vector<int> &);
void print(vector<string> &my_vec);

int main (){
    
    vector<int> my_vec {1,2,3,4,5}; 
    vector<string> my_data {}; 
    print(my_vec); 

    string temp; 
    cout << "Enter a string: "; 
    cin >> temp;

    my_data.push_back(temp); 
    print(my_data);  

    return 0;
}
void print(vector<int> &my_vec){
    for (auto val : my_vec)
        cout << val << endl; 
}
void print(vector<string> &my_vec){
    for (auto val : my_vec)
        cout << val << endl; 
}
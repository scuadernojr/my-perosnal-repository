#include <iostream>
#include <string>
using namespace std;

bool is_4_digits (string PIN){

    bool status = true;
    if (PIN.length() != 4){
        status = false; 
    }
    return status; 

}
int main(){
    
    string PIN; 
    
    cout << "Enter a pin ";
    cin >> PIN ; 
    
    if (is_4_digits(PIN) == 1)
        cout << "Pin accepted!" << endl;
    else {
        cout << "Pin Denied" << endl; 
    }
}
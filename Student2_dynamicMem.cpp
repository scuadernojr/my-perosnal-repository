#include <iostream>
#include <string> 
using namespace std; 

class Student{
    public:
    int roll_number; 
    string phone_number; 
    string address; 


}; 
int main(){
    
    Student John;  
    John.roll_number = 1; 
    John.phone_number = "831445550"; 
    John.address = "123 Alameda Drive";

    cout << "Johns roll number is " << John.roll_number << ". His phone number is "
    << John.phone_number << " and his address is " << John.address << "." << endl;
    
    //creating in heap using pointer 
    Student *Sam = new Student;
    Sam->roll_number = 2; 
    Sam->phone_number = "8325467786";
    Sam->address = "145 Silacci Road";
    
    cout << "Sams roll number is " <<  Sam->roll_number << ". His phone number is " << Sam->phone_number
        << "  and his address is " << Sam->address << "." << endl;
    delete Sam; 
}

#include <iostream>

using namespace std;

int main() {
    
    const int dollar_value {100};
    const int quarter_value {75}; 
    const int dime_value {10}; 
    const int nickle_value {5}; 
    
    int cents {}; 
    
    cout << "Enter the amount of cents "; 
    cin >> cents; // if entered 267 cents 
    
    int balance {}, dollars {}, quarters {}, dimes {}, nickles {}, pennies {}; 
    
    balance = dollars = quarters = dimes = nickles = pennies = 0; // Do not need but initialized 
    
    //====================================================================================================================
    
    dollars = cents/dollar_value;        // 267 / 100 =                      TWO dollars 
    balance = cents % dollar_value;      // 267 % 100 REMAINDER 67 
    
    quarters = balance / quarter_value;  // balance is 67 / 25 =             TWO Quarters 
    balance %=  quarter_value;            // balance is 67 % 25  REMAINDER 17
    
    dimes = balance / dime_value; 
    balance %= dime_value; 
    
    nickles = balance / nickle_value; 
    balance %= nickle_value; 
    
    pennies = balance; 
    
    cout << "Dollars: " << dollars << endl; 
    cout << "Quarters: " << quarters << endl;
    cout << "Dimes: " << dimes << endl; 
    cout << "Nickles: " << nickles << endl;
    cout << "Pennies: " << pennies << endl; 
    
  return 0;
}
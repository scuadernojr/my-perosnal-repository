#include <iostream>
using namespace std; 
class Shallow{
private:
    int *data; 
public:
    void set_data_value (int d) {*data = d;}
    int get_data_value () {return *data; }

    //Constructor 
    Shallow (int d); 
    //Copy COnstructor 
    Shallow (const Shallow &souce); 
    //Destructor
    ~Shallow(); 
};
Shallow::Shallow(int d){
    data = new int; 
    *data = d; 
}

Shallow::Shallow (const Shallow &source)
:data(source.data){
    cout << "Copy Constructor -Shallow Copy" << endl; 
}

Shallow::~Shallow(){
    delete data; 
    cout <<"Deconstructor freeing data." << endl; 
}
void display_shallow(Shallow s){
    cout << s.get_data_value() << endl; 
}
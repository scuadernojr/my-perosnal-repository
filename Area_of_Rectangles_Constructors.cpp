#include <iostream>
#include <cmath> 
#include <iomanip> 
using namespace std;

class Rectangle{
    public: 
    int x,y; // can make private 

    Rectangle(int s1, int s2)
              :x(s1), y(s2) {}; 
    
    void print_area()
    {
        double area = x * y; 
        cout << "Area of retangle is " << area << endl; 
    }

}; 

int main(){
    Rectangle myRec1(4,5);
    Rectangle myRec2(5,8); 
    
    cout << "Area of rectangle with length 4 and width 5 is " << endl; 
    myRec1.print_area();
    
    cout << "\n"; 
    
    cout << "Area of rectangle with length 5 and width 8 is " << endl; 
    myRec2.print_area();
    
    return 0;
}
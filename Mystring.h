#ifndef MYSTRING_H
#define MYSTRING_H

#pragma once

class Mystring
{
    friend bool operator==(const Mystring &lhs, const Mystring &rhs); 
    friend Mystring operator-(const Mystring &obj); 
    friend Mystring operator+(const Mystring &lhs, const Mystring &rhs); 

private:
    char *str; //c-style sting pointer 
public:
    Mystring();// no args constructor
    Mystring(const char *s); // overloaded constructor
    Mystring (const Mystring &source); //copy constructor with &
    Mystring (Mystring &&source); // MOVE constrctor 
    ~Mystring(); // Destructor

    Mystring &operator= (const Mystring &rhs); //COPY Assignment 
    Mystring &operator= (Mystring &&rhs); //Move operator 
    
    Mystring operator-() const; //Make lowercase
    Mystring operator+(const Mystring &rhs) const; //concatanate
    bool operator==(const Mystring &rhs) const; 

    void display()const; 
    int get_length() const; //getters
    const char *get_string() const; //getters


};

#endif// MYSTRING_H
#include <iostream> 
#include <string> 
using namespace std; 

class Student{
    public:
        Student (string a , int b): name(a) , age(b){}
        void set_name(string a){name = a; }
        void get_name(){cout << "Name: " << name << endl; }
    private: 
        string name; 
        int age; 

}; 

int main(){
    Student stud1 ("Sam", 49); 
    get_name(); 
    
    return 0;
}
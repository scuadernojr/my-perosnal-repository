#include "Mystring.h"
#include <cstring>
#include <iostream>

//No Args constructor 
Mystring::Mystring()
:str{nullptr}{
    std::cout << "No Args COnstructor" << std::endl;
    str = new char[1]; // allocate one character in the HEAP 
    *str = '\0'; 
}

//Overlaoded constructor 
Mystring::Mystring(const char *s)
:str{nullptr}{
    std::cout << "Overloaded COnstructor." << std::endl; 
    if (s==nullptr){
        str = new char[1]; 
        *str = '\0'; 
    }else{
        str = new char[std::strlen(s)+1];
        std::strcpy(str,s); // copy s into str 
    }
}
//copy Constructor
Mystring::Mystring(const Mystring &source)
:str{nullptr}{
    std::cout << "Copy Constructor" << std::endl; 
    str = new char[std::strlen(source.str)+1];
    std::strcpy(str, source.str);
}
//Move constructor
Mystring::Mystring (Mystring &&source )
    :str{source.str}{
    source.str = nullptr; 
    std::cout << "Move constructor mused" << std::endl; 
}
//Deconstructor
Mystring::~Mystring(){
    if (str == nullptr)
        std::cout << "calling destructor for Mystring nullptr" << std::endl; 
    else{
        std::cout << "Calling destructor for Mystring:" << str <<std::endl;
    }
    delete [] str; 
}

//Copy assignment 
Mystring &Mystring::operator=(const Mystring &rhs){
    std::cout << "Using copy assignment" << std::endl; 
    if (this == &rhs)
        return *this;
    delete [] str; 
    str = new char[std::strlen(rhs.str)+1];
    std::strcpy(str, rhs.str); 
    return *this;
}

//Move assignment 
Mystring &Mystring::operator = (Mystring &&rhs){
    std::cout << "Using move assignment" << std::endl; 
    if (this == &rhs) // Check for self assignment
    return *this; 
    delete [] str; // clear the HEAP 
    str = rhs.str; // steal the pointer 
    rhs.str = nullptr; // nullify the other pointer 
    return *this; 

}
//Equality
bool Mystring::operator==(const Mystring &rhs) const{
    return(std::strcmp(str,rhs.str) ==0 );
}
//Make lowercase Operator 
Mystring Mystring::operator-()const {
    char *buff = new char[std::strlen(str)+1]; 
    std::strcpy(buff,str); 
    for (size_t i = 0; i <std::strlen(buff); i++)
        buff[i] = std::tolower(buff[i]); 
    Mystring temp {buff}; 
    delete [] buff;
    return temp; 
}
//Concatanate 
Mystring Mystring::operator+(const Mystring &rhs) const {
    char *buff = new char[std::strlen(str)+ std::strlen(rhs.str)+ 1]; 
    std::strcpy(buff,str);
    std::strcat(buff,rhs.str); 
    Mystring temp {buff}; 
    delete [] buff; 
    return temp; 
}
//Display method
void Mystring::display() const {
    std::cout << str << ":" << get_length() << std::endl; 
}

//Length getter
int Mystring::get_length() const {return std::strlen(str);}

//String getter
const char *Mystring::get_string() const {return str; }

//equality regular function not member function 
bool operator==(const Mystring &lhs, const Mystring &rhs){
    return (std::strcmp(lhs.str,rhs.str) == 0);
}

//Make lowercase 
Mystring operator-(const Mystring &obj){
    char *buff = new char[std::strlen(obj.str)+1]; 
    std::strcpy(buff,obj.str); 
    for (size_t i = 0; i <std::strlen(buff); i++)
        buff[i] = std::tolower(buff[i]);
        Mystring temp {buff}; 
        delete [] buff; 
        return temp; 
}

//concatanation 
Mystring operator+(const Mystring &lhs, const Mystring &rhs){
    char *buff = new char[std::strlen(lhs.str)+ std::strlen(rhs.str)+1];
    std::strcpy(buff,lhs.str); 
    std::strcat(buff,rhs.str); 
    Mystring temp {buff}; 
    delete [] buff; 
    return temp; 
}
#include <iostream>

using namespace std;

int main() {
    
    int num1 {}, num2 {};
    
    cout << boolalpha; // true/false instead of 1 and 0 
    
    cout << "Enter two numbers "; 
    cin >> num1 >> num2; 
    
    cout << num1 << " > " << num2 << ": " << (num1 > num2) << endl;
    cout << num1 << " < " << num2 << ": " << (num1 < num2) << endl; 
    
  return 0;
}
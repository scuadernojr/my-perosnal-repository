#include <iostream>
#include <string> 
using namespace std; 
int main(){
    string s1 = "Sammy"; 
    string s2 {"Kaitlyn"}; 
    string *ptr {&s1};

    string my_string_array [] {"Sammy", "Kaitlyn", "Sheilla"};
    string *string_ptr {nullptr}; 
    string_ptr = &my_string_array[0];// point to index 2 

    cout << s1 << " and " << s2 << endl; 
    cout << *ptr << endl; // Sammy
    cout << ptr << endl; // address

    cout << my_string_array[2] << endl; // SHeilla 
    cout << *string_ptr << endl; // Sammy
    cout << "==================PTR Artihmatic===============" << endl; 
    cout << *string_ptr++ << endl;// Sammy  
    cout << *string_ptr++ << endl; // Kaitlyn 
    cout << *string_ptr++ << endl; // Sheilla 

    return 0;
}